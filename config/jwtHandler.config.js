'use strict';

const env = require('./../config/env.config');
const jwt = require('jsonwebtoken');

module.exports.token = (input) => {
  try {
    const token = jwt.sign(input, env.jwtSecret, {
      algorithm: 'HS256',
      expiresIn: env.jwtExpiresIn,
    });
    return token;
  } catch (ex) {
    throw ex;
  }
};
