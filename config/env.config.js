"use strict";

module.exports = {
  prod: process.env.NODE_AB_PROD || 0,
  port: process.env.NODE_AB_PORT || 3000,
  aesKEY:  process.env.NODE_AB_AES_KEY || "346c1208d7065b70",
  aesIV:  process.env.NODE_AB_AES_IV || "ba7d2f541febfd8a",
  jwtSecret:  process.env.NODE_AB_JWT_SECRET || "f72544bcac4416ba2edc8c5ca5db63233e41dfc5aa8c167cdbe29293676de69b",
  jwtExpiresIn:  process.env.NODE_JWT_EXPIRY || "1h", // One Hour,
  exceptionStatus: 500,
  MB: 1048576,
  projectPath :  process.env.NODE_AB_PORT || '/var/www/booksAppBackend/',
  videoChunkSize: 1024 * 1024
};
