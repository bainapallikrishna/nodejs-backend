"use strict";
const authbookdb = require("./../db/authbook.db");
const env = require("./../config/env.config");
const aes = require("./../utils/aes.utils");
const crypto = require("crypto");


module.exports.userRegSub = async (req, res, next) => {
  try {
    if (
      !req.body.firstName ||
      !req.body.lastName ||
      !req.body.gender ||
      !req.body.email ||
      !req.body.mobileNumber ||
      !req.body.dob ||
      !req.body.state ||
      !req.body.password ||
      !req.body.role ||
      !req.body.pinCode
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const passwordHash = crypto
      .createHash("sha256")
      .update(req.body.password)
      .digest("hex");

    const result = await authbookdb.connection(
      `call authbook.user_reg_ins('${req.body.firstName}','${req.body.lastName}','${req.body.gender}','${req.body.email}','${req.body.mobileNumber}','${req.body.dob}','${req.body.state}','${passwordHash}','${req.body.role}','${req.body.pinCode}','${req.body.publicationName}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "User Details Submitted Successfully !!!",
      });
    } else if (result.length > 0 && result[0].status === 2) {
      res.status(400).json({
        result: result,
        message: "User Details Already Submitted !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Submit User Details, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.registerBookToUser = async (req, res, next) => {
  try {
    if (
      !req.body.userId ||
      !req.body.bookPurchasedDate ||
      !req.body.qrId ||
      !req.body.updateBy
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const uniqueId = aes.decrypt(req.body.qrId);

    const result = await authbookdb.connection(
      `call authbook.book_purchase_update('${req.body.userId}','${req.body.bookPurchasedDate}','${req.body.updateBy}','${uniqueId}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: [],
        message: "Book Registered Successfully !!!",
      });
    } else if (result.length > 0 && result[0].status === 2) {
      res.status(400).json({
        result: [],
        message: "QR Code Already Used !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Register Book !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.userPurchasedBooksList = async (req, res, next) => {
  try {
    if (!req.body.userId) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.user_purchased_books_get('${req.body.userId}')`
    );

    if (result.length > 0) {
      // const bookList = [];
      // for (let i = 0; i < result.length; i++) {
      //   bookList.push({
      //     author_name: result[i].author_name,
      //     book_description: result[i].book_description,
      //     book_id: result[i].book_id,
      //     book_name: result[i].book_name,
      //     book_purchased_date: result[i].book_purchased_date,
      //     cover_photo: result[i].cover_photo,
      //     language_name: result[i].language_name,
      //     price: result[i].price,
      //     published_on: result[i].published_on,
      //     serial_number: result[i].serial_number,
      //     unique_id: aes.encrypt(result[i].unique_id),
      //   });
      // };
      res.status(200).json({
        result: result,
        message: "Books List Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No Books Found In Your Account !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.passwordUpdate = async (req, res, next) => {
  try {
    if (
      !req.body.oldPassword ||
      !req.body.newPassword ||
      !req.body.userId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const oldPassword = crypto
      .createHash("sha256")
      .update(req.body.oldPassword)
      .digest("hex");
    const newPassword = crypto
      .createHash("sha256")
      .update(req.body.newPassword)
      .digest("hex");

    const result = await authbookdb.connection(
      `call authbook.user_password_update('${oldPassword}','${newPassword}','${req.body.userId}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Password Updated Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Update Password, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.certificateDetailsByUniqueId = async (req, res, next) => {
  try {
    if (
      !req.body.qrId 
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const uniqueId = aes.decrypt(req.body.qrId);

    const result = await authbookdb.connection(`call authbook.auth_cert_details_get('${ uniqueId }')`);

    if (result.length > 0 ) {
      res.status(200).json({
        result: result[0],
        message: "Certificate Details Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Certificate Details, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.filesListByBookId = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.fileType
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.file_management_get('${req.body.fileType}','${req.body.bookId}')`
    );

    if (result.length > 0) {
      
      if(req.body.fileType === '2'){

        let encryptedString = [];
        for (let i = 0; i < result.length; i++) {
          encryptedString.push({
            book_name: result[i].book_name,
            file_type: result[i].file_type,
            file_path: aes.encrypt(result[i].file_path),
            created_on: result[i].created_on,
            description: result[i].description,
            file_id: result[i].file_id,
            publisher_id: result[i].publisher_id,
          });
        }
  
        res.status(200).json({
          result: encryptedString,
          message: "Files Loaded Successfully !!!",
        });

      }else {
        
        res.status(200).json({
          result: result,
          message: "Files Loaded Successfully !!!",
        });

      }
    } else {
      res.status(400).json({
        result: [],
        message: "No Files Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.ratingSub = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.userId ||
      !req.body.rating
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.book_rating_ins('${req.body.bookId}','${req.body.userId}','${req.body.rating}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Your Rating has been Submitted Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Submit Rating, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.commentSub = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.userId ||
      !req.body.comment
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.book_comments_ins('${req.body.bookId}','${req.body.userId}','${req.body.comment}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Your Comment has been Submitted Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Submit Comment, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.commentUpdate = async (req, res, next) => {
  try {
    if (
      !req.body.bookId  ||
      !req.body.userId  ||
      !req.body.comment 
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.book_comments_update('${req.body.bookId}','${req.body.userId }','${req.body.comment }')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Comments Updated Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Update Comments, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.ratingUpdate = async (req, res, next) => {
  try {
    if (
      !req.body.bookId  ||
      !req.body.userId  ||
      !req.body.rating  
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.book_rating_update('${req.body.bookId}','${req.body.userId }','${req.body.rating}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Book Rating Updated Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Update Book Rating, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.commentsList = async (req, res, next) => {
  try {
    if (
      !req.body.bookId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.book_first_ten_comments_get('${req.body.bookId}')`
    );
    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Comments Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Comments Not Available !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};