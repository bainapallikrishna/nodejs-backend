"use strict";
const authbookdb = require("./../db/authbook.db");
const env = require("./../config/env.config");
const files = require("./../utils/files.utils");
const aes = require("./../utils/aes.utils");
const commonHelper = require("../utils/helper.utils");
const path = require("path");
const fs = require("fs");


module.exports.encrypt = async function (req, res, next) {
  try {
    const text = req.body.sample;
    const result = aes.encrypt(text);
    res.status(200).json({
      result: result,
      message: "Data Encrypted Successfully !!!",
    });

  } catch (error) {
    if (error)
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
    next(error);
  }
};

module.exports.decrypt = async function (req, res, next) {
  try {
    const encryptedText = req.body.encryptedText;
    const result = aes.decrypt(encryptedText);
    res.status(200).json({
      result: result,
      message: "Data Decrypted Successfully !!!",
    });

  } catch (error) {
    if (error)
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
    next(error);
  }
};

module.exports.saveImage = async function (req, res, next) {
  try {
    const baseFile = req.body.baseFile;
    const folderName = "testImages";
    const response = await files.saveImage(baseFile, folderName);
    if (response) {
      res.status(200).json({
        result: response,
        decodedToken: req.decodedToken,
        message: "Image Saved Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Save Image !!!",
      });
    }
  } catch (error) {
    if (error)
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
    next(error);
  }
};

module.exports.videoUpload = async function (req, res, next) {
  try {
    const file = req.file;
    if (!file) {
      const error = new Error("Please upload a file");
      error.status = 400;
      return next(error);
    }
    if (file.size > (20 * env.MB)) {
      const error = new Error("File is too large !!!, Please upload video below 20MB only !!!");
      error.status = 400;
      return next(error);
    }
    const path = file.path.toString();
    const filePath = path.replace(/\\/g, "//");
    res.status(200).json({
      result: filePath,
      decodedToken: req.decodedToken,
      message: "Video Saved Successfully !!!",
    });

  } catch (error) {
    if (error)
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
    next(error);
  }
};

module.exports.getBatchId = async (req, res, next) => {
  try {
    if (
      !req.body.batchId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
      return;
    }

    let count = 0;
    const result = [];
    let tempId = req.body.batchId;
    for (let i = 0; i < 1000000; i++) {
      const batchId = await commonHelper.getBatchId(tempId);
      const serialNo = await commonHelper.getSerialNo(batchId);
      const uniqueId = await commonHelper.getUniqueId(serialNo);
      const response = await authbookdb.connection(`call authbook.book_unique_id_details_ins('${uniqueId}','${serialNo}')`);
      if (response.length > 0 && response[0].status === 1) {
        result.push({
          BATCH_ID: batchId,
          SERIAL_NO: serialNo,
          UNIQUE_ID: uniqueId
        });
        tempId = batchId;
        count++;
        console.log('Records Inserted : ' + count);
      }

    }

    res.status(200).json({
      result: result,
      message: "Batch Created Successfully !!!",
      totalInserted: count
    });

  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.getVideo = async (req, res, next) => {
  try {

    if (!req.query.request) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }


    let decFile = aes.decrypt(req.query.request.replace(' ', '+'));
    if (decFile) {
      if (env.prod === 1) {
        decFile = env.projectPath + decFile;
      }
      const filepath = path.resolve(decFile);
      const stat = fs.statSync(filepath);
      const fileSize = stat.size;
      const range = req.headers.range;
      if (range) {
        const parts = range.replace(/bytes=/, "").split("-");
        const start = parseInt(parts[0], 10);
        let end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
        let chunksize = (end - start) + 1;

        // poor hack to send smaller chunks to the browser
        const maxChunk = env.videoChunkSize;
        if (chunksize > maxChunk) {
          end = start + maxChunk - 1;
          chunksize = (end - start) + 1;
        }

        const file = fs.createReadStream(filepath, { start, end });
        const head = {
          'Content-Range': `bytes ${start}-${end}/${fileSize}`,
          'Accept-Ranges': 'bytes',
          'Content-Length': chunksize,
          'Content-Type': 'video/mp4',
        }
        console.log('head', head);
        res.writeHead(206, head);
        file.pipe(res);
      } else {
        const head = {
          'Content-Length': fileSize,
          'Content-Type': 'video/mp4',
        }
        res.writeHead(200, head);
        fs.createReadStream(path).pipe(res);
      }

    } else {
      res.status(400).json({
        result: [],
        message: "Video Corrupted, Please try again !!!",
      });
    }

  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.filesListByBookId = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.fileType
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.file_management_get('${req.body.fileType}','${req.body.bookId}')`
    );

    if (result.length > 0) {

      let encryptedString = [];
      for (let i = 0; i < result.length; i++) {
        encryptedString.push({
          book_name: result[i].book_name,
          file_type: result[i].file_type,
          file_path: aes.encrypt(result[i].file_path),
          created_on: result[i].created_on,
          description: result[i].description,
          file_id: result[i].file_id,
          publisher_id: result[i].publisher_id,
        });
      }

      res.status(200).json({
        result: encryptedString,
        message: "Files Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No Files Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};