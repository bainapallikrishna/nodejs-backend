"use strict";
const authbookdb = require("./../db/authbook.db");
const env = require("./../config/env.config");
const files = require("./../utils/files.utils");
const aes = require("./../utils/aes.utils");
const commonHelper = require("../utils/helper.utils");
const fs = require('fs');

module.exports.getBooksList = async (req, res, next) => {
  try {
    const result = await authbookdb.connection(
      `call authbook.book_details_get()`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Books Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No Books Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.authorSub = async (req, res, next) => {
  try {
    if (
      !req.body.authorName ||
      !req.body.mobileNumber ||
      !req.body.email ||
      !req.body.gender ||
      !req.body.insertedBy
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.author_details_ins('${req.body.authorName}','${req.body.gender}','${req.body.mobileNumber}','${req.body.email}','${req.body.insertedBy}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Author Created Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Create Author, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.authorList = async (req, res, next) => {
  try {
    const result = await authbookdb.connection(
      `call authbook.author_details_get()`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Authors List Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Authors Not Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.languageMaster = async (req, res, next) => {
  try {
    const result = await authbookdb.connection(
      `call authbook.language_master_get()`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Languages Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Languages Not Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.bookSub = async (req, res, next) => {
  try {
    if (
      !req.body.bookName ||
      !req.body.authorName ||
      !req.body.price ||
      !req.body.publishedOn ||
      !req.body.languageId ||
      !req.body.insertedBy ||
      !req.body.description ||
      !req.body.publisherName ||
      !req.body.isbnNo ||
      !req.body.noOfPages ||
      !req.body.translatorName ||
      !req.body.editorName ||
      !req.body.priceDenomination ||
      !req.body.companyName
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.book_details_ins('${req.body.bookName}','${req.body.authorName}','${req.body.price}','${req.body.publishedOn}','${req.body.languageId}','${req.body.insertedBy}','${req.body.description}','${req.body.publisherName}','${req.body.isbnNo}','${req.body.noOfPages}','${req.body.translatorName}','${req.body.editorName}','${req.body.priceDenomination}','${req.body.companyName}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Books Submitted Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Submit Book Details, Please Try Again !!!",
      });
    }

  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.bookQRGenerationSub = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.insertedBy ||
      !req.body.noOfCopies
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.book_transactions_unique_id_ins('${req.body.bookId}','${req.body.noOfCopies}','${req.body.insertedBy}')`
    );
    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "QR Codes Generated Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Generate QR Codes, Please Try Again !!!",
      });
    }


  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.batchListByBookId = async (req, res, next) => {
  try {
    if (
      !req.body.bookId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.book_id_wise_batch_details_get('${req.body.bookId}')`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Batch List Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No Batches Found, Please Generate QR Codes !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.QRCodesListByBatchId = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.batchCopyId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.book_transactions_get('${req.body.bookId}','${req.body.batchCopyId}')`
    );

    if (result.length > 0) {
      let encryptedString = [];
      for (let i = 0; i < result.length; i++) {
        encryptedString.push({
          book_id: result[i].book_id,
          serial_number: result[i].serial_number,
          unique_id: aes.encrypt(result[i].unique_id)
        });
      }
      res.status(200).json({
        result: encryptedString,
        message: "QR Codes Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No QR Codes Found, Please Generate QR Codes !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.imageUpload = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.baseFile ||
      !req.body.insertedBy ||
      !req.body.publisherId ||
      !req.body.description
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const baseFile = req.body.baseFile;
    const folderName = "bookImages";
    const filePath = await files.saveImage(baseFile, folderName);

    const result = await authbookdb.connection(
      `call authbook.file_management_ins('${req.body.bookId}','0','${filePath}','1','${req.body.insertedBy}','${req.body.publisherId}','${req.body.description}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Photo Uploaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Upload Photo, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.docUpload = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.baseFile ||
      !req.body.insertedBy ||
      !req.body.publisherId ||
      !req.body.description
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const baseFile = req.body.baseFile;
    const folderName = "bookDocs";
    const filePath = await files.saveDoc(baseFile, folderName);

    const result = await authbookdb.connection(
      `call authbook.file_management_ins('${req.body.bookId}','1','${filePath}','1','${req.body.insertedBy}','${req.body.publisherId}','${req.body.description}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Document Uploaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: result,
        message: "Failed To Upload Document, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.videoUpload = async (req, res, next) => {
  try {

    if (
      !req.body.bookId ||
      !req.body.insertedBy ||
      !req.body.publisherId ||
      !req.body.description
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const file = req.file;
    if (!file) {
      const error = new Error("Please upload a file");
      error.status = 400;
      return next(error);
    }
    if (file.size > (20 * env.MB)) {
      const error = new Error("File is too large !!!, Please upload video below 20MB only !!!");
      error.status = 400;
      return next(error);
    }
    const path = file.path.toString();
    const filePath = path.replace(/\\/g, "/");
    const relativePath = filePath.replace(env.projectPath, "");

    const result = await authbookdb.connection(
      `call authbook.file_management_ins('${req.body.bookId}','2','${relativePath}','1','${req.body.insertedBy}','${req.body.publisherId}','${req.body.description}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "Video Uploaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Upload Video, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.filesListByBookId = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.fileType
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.file_management_get('${req.body.fileType}','${req.body.bookId}')`
    );

    if (result.length > 0) {

      if (req.body.fileType === '2') {

        let encryptedString = [];
        for (let i = 0; i < result.length; i++) {
          encryptedString.push({
            book_name: result[i].book_name,
            file_type: result[i].file_type,
            file_path: aes.encrypt(result[i].file_path),
            created_on: result[i].created_on,
            description: result[i].description,
            file_id: result[i].file_id,
            publisher_id: result[i].publisher_id,
          });
        }

        res.status(200).json({
          result: encryptedString,
          message: "Files Loaded Successfully !!!",
        });

      } else {

        res.status(200).json({
          result: result,
          message: "Files Loaded Successfully !!!",
        });

      }

    } else {
      res.status(400).json({
        result: [],
        message: "No Files Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.bookListByPublisherId = async (req, res, next) => {
  try {
    if (
      !req.body.publisherId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.publisher_wise_book_details_get('${req.body.publisherId}')`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Books Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No Books Found, Please Add Books !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.certificateDetailsByUniqueId = async (req, res, next) => {
  try {
    if (
      !req.body.uniqueId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const uniqueId = aes.decrypt(req.body.uniqueId);
    const result = await authbookdb.connection(
      `call authbook.auth_cert_details_get('${uniqueId}')`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Details Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Book Not Registered To User !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.coverPicsUpload = async (req, res, next) => {
  try {
    if (
      !req.body.bookId ||
      !req.body.baseFiles
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    let uploadedFilesCount = 0;
    for (let i = 0; i < req.body.baseFiles.length; i++) {
      const baseFile = req.body.baseFiles[i];
      const folderName = "coverPics";
      const filePath = await files.saveImage(baseFile, folderName);
      if (filePath) {
        const result = await authbookdb.connection(`call authbook.book_cover_page_ins('${req.body.bookId}','${filePath}')`);
        if (result) {
          if (result.length > 0 && result[0].status === 1) {
            uploadedFilesCount++;
          }
        }
      }
    }

    if (uploadedFilesCount === req.body.baseFiles.length) {
      res.status(200).json({
        result: [],
        message: "Cover Photo Uploaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Failed To Upload All Cover Photos, Please Try Again !!!",
      });
    }
  } catch (error) {
    console.log(error);
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.deleteFileById = async (req, res, next) => {
  try {
    if (
      !req.body.bookId,
      !req.body.fileId,
      !req.body.filePath
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    let filePath = aes.decrypt(req.body.filePath);
    filePath = files.getAbsolutePath(filePath);

    const fileResponse = await files.fileDelete(filePath);
    if (fileResponse === '1' || fileResponse.errno === -2) {
      const result = await authbookdb.connection(
        `call authbook.delete_media_data('${req.body.bookId}','${req.body.fileId}')`
      );

      if (result.length > 0 && result[0].status === 1) {
        res.status(200).json({
          result: fileResponse,
          message: "File Deleted Successfully !!!",
        });
      } else {
        res.status(400).json({
          result: result,
          message: "Failed To Delete File, Please try again !!!",
        });
      }
    } else {
      res.status(400).json({
        result: fileResponse,
        message: "Failed To Delete File, Please try again !!!",
      });
    }


  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.commentsList = async (req, res, next) => {
  try {
    if (
      !req.body.bookId
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const result = await authbookdb.connection(
      `call authbook.book_first_ten_comments_get('${req.body.bookId}')`
    );
    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Comments Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "Comments Not Available !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

