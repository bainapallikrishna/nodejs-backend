"use strict";

const authbookdb = require("./../db/authbook.db");
const env = require("./../config/env.config");
const jwtHandler = require("./../config/jwtHandler.config");
const crypto = require('crypto');
const aes = require("./../utils/aes.utils");



module.exports.getBookDetailsByUniqueId = async (req, res, next) => {
  try {
    if (!req.body.uniqueId) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }
    const uniqueId = aes.decrypt(req.body.uniqueId);
    const result = await authbookdb.connection(
      `call authbook.book_uniq_code_details_get('${uniqueId}')`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "Books Details Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No Books Found For Scanned QR Code !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.verifyUser = async (req, res, next) => {
  try {
    if (
      !req.body.mobileNumber
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.login_reg_user_check_get('${req.body.mobileNumber}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      res.status(200).json({
        result: result,
        message: "User Details Found !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "User Details Not Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.sendOtp = async (req, res, next) => {
  try {
    if (
      !req.body.mobileNumber ||
      !req.body.captchaValue ||
      !req.body.captchaCipher
    ) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const captchaDB = await authbookdb.connection(
      `call authbook.login_captcha_get('${req.body.captchaValue}','${req.body.captchaCipher}')`
    );

    if (captchaDB.length > 0 && captchaDB[0].status === 1) {
      const otp = "123456";

      const result = await authbookdb.connection(
        `call authbook.login_otp_ins('${req.body.mobileNumber}','${otp}')`
      );

      if (result.length > 0 && result[0].status === 1) {
        res.status(200).json({
          result: result,
          message: "OTP sent to registered mobile number !!!",
        });
      } else {
        res.status(400).json({
          result: [],
          message: "OTP sending Failed, Please try again !!!",
        });
      }
    } else {
      res.status(400).json({
        result: captchaDB,
        message: "Captcha Expired !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.verifyOtp = async (req, res, next) => {
  try {
    if (!req.body.mobileNumber || !req.body.otp) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const result = await authbookdb.connection(
      `call authbook.login_otp_get('${req.body.mobileNumber}','${req.body.otp}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      const token = jwtHandler.token({
        name: result[0].first_name,
        email: result[0].email,
        role: result[0].role,
      });
      if (token) {
        res.status(200).json({
          result: token,
          role: result[0].role,
          message: "User Logged In Successfully !!!",
        });
      } else {
        res.status(400).json({
          result: "",
          message: "Failed to generate token, Please Try Again !!!",
        });
      }
    } else {
      res.status(400).json({
        result: [],
        message: "OTP Expired, Please Try Again !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.userLogin = async (req, res, next) => {
  try {

    if (!req.body.mobileNumber || !req.body.password) {
      res.status(400).json({
        result: [],
        message: "Input is Empty !!!",
      });
    }

    const passwordHash = crypto.createHash('sha256').update(req.body.password).digest('hex');

    const result = await authbookdb.connection(
      `call authbook.user_login_get('${req.body.mobileNumber}','${passwordHash}')`
    );

    if (result.length > 0 && result[0].status === 1) {
      const token = jwtHandler.token({
        name: result[0].first_name,
        email: result[0].email,
        role: result[0].role,
      });
      if (token) {
        res.status(200).json({
          token: token,
          user_id: result[0].user_id,
          email: result[0].email,
          gender: result[0].gender,
          role: result[0].role,
          message: "User Logged In Successfully !!!",
          firstName: result[0].first_name,
          lastName: result[0].last_name,
          profile_pic: result[0].profile_pic,
          state: result[0].state,
          dob_dt: result[0].dob_dt
        });
      } else {
        res.status(400).json({
          result: "",
          message: "Failed to generate token, Please Try Again !!!",
        });
      }
    } else {
      res.status(400).json({
        result: [],
        message: "Invalid Username or Password !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};

module.exports.stateList = async (req, res, next) => {
  try {
    const result = await authbookdb.connection(
      `call authbook.india_state_get()`
    );

    if (result.length > 0) {
      res.status(200).json({
        result: result,
        message: "States Loaded Successfully !!!",
      });
    } else {
      res.status(400).json({
        result: [],
        message: "No States Found !!!",
      });
    }
  } catch (error) {
    if (error) {
      if (!error.status) {
        error.status = env.exceptionStatus;
      }
      next(error);
    }
  }
};