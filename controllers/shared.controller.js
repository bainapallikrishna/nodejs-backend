"use strict";
const authbookdb = require("./../db/authbook.db");
const env = require("./../config/env.config");
const aes = require("./../utils/aes.utils");
const path = require("path");
const fs = require("fs");
const files = require("./../utils/files.utils");


module.exports.getVideo = async (req, res, next) => {
    try {
  
      if (!req.query.request) {
        res.status(400).json({
          result: [],
          message: "Input is Empty !!!",
        });
      }
  
  
      let decFile = aes.decrypt(req.query.request.replace(' ', '+'));
      if (decFile) {
        if(env.prod === 1){
          decFile = env.projectPath + decFile;
        }
        const filepath = path.resolve(decFile);
        const stat = fs.statSync(filepath);
        const fileSize = stat.size;
        const range = req.headers.range;
        if (range) {
          const parts = range.replace(/bytes=/, "").split("-");
          const start = parseInt(parts[0], 10);
          let end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;
          let chunksize = (end - start) + 1;
  
          // poor hack to send smaller chunks to the browser
          const maxChunk = env.videoChunkSize;
          if (chunksize > maxChunk) {
            end = start + maxChunk - 1;
            chunksize = (end - start) + 1;
          }
  
          const file = fs.createReadStream(filepath, { start, end });
          const head = {
            'Content-Range': `bytes ${start}-${end}/${fileSize}`,
            'Accept-Ranges': 'bytes',
            'Content-Length': chunksize,
            'Content-Type': 'video/mp4',
          }
          console.log('head', head);
          res.writeHead(206, head);
          file.pipe(res);
        } else {
          const head = {
            'Content-Length': fileSize,
            'Content-Type': 'video/mp4',
          }
          res.writeHead(200, head);
          fs.createReadStream(path).pipe(res);
        }
  
      } else {
        res.status(400).json({
          result: [],
          message: "Video Corrupted, Please try again !!!",
        });
      }
  
    } catch (error) {
      if (error) {
        if (!error.status) {
          error.status = env.exceptionStatus;
        }
        next(error);
      }
    }
  };

module.exports.profilePicUpdateByUserId = async (req, res, next) => {
    try {
      if (
        !req.body.userId ||
        !req.body.profilePic
      ) {
        res.status(400).json({
          result: [],
          message: "Input is Empty !!!",
        });
      }
  
      const baseFile = req.body.profilePic;
      const folderName = "profilePics";
      const coverPhoto = await files.saveImage(baseFile, folderName);
      if (coverPhoto) {
        const result = await authbookdb.connection(
          `call authbook.user_profile_pic_update('${req.body.userId}','${coverPhoto}')`
        );
  
        if (result.length > 0 && result[0].status === 1) {
          res.status(200).json({
            result: coverPhoto,
            message: "Profile Picture Updated Successfully !!!",
          });
        } else {
          res.status(400).json({
            result: [],
            message: "Failed To Update Profile Picture, Please Try Again !!!",
          });
        }
      } else {
        res.status(400).json({
          result: [],
          message: "Failed Update Profile Pic, Please Try Again !!!",
        });
      }
    } catch (error) {
      if (error) {
        if (!error.status) {
          error.status = env.exceptionStatus;
        }
        next(error);
      }
    }
  };
  