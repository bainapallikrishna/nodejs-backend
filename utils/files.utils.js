"use strict";
const fs = require("fs");
const date = require("./date.utils");
const env = require("./../config/env.config");
const multer = require("multer");
const { resolve } = require("path");

module.exports.saveImage = (baseFile, folderName) => {
  return new Promise((resolve, reject) => {
    let path = this.getPath("Images", folderName);

    if (!path || path[0] == 'undefined') reject(err);
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true }, (err) => {
        reject(err);
      });
    }

    const fileName = date.getTimeStamp() + ".jpg";
    fs.writeFile(path + fileName, baseFile, "base64", function (err) {
      if (err) {
        console.log(err);
        reject(err);
      }
      const relativePath = path.replace(env.projectPath, "");
      resolve(relativePath + fileName);
    });
  });
};

module.exports.saveDoc = (baseFile, folderName) => {
  return new Promise((resolve, reject) => {
    let path = this.getPath("Documents", folderName);

    if (!path || path[0] == 'undefined') reject(err);
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true }, (err) => {
        reject(err);
      });
    }

    const fileName = date.getTimeStamp() + ".pdf";
    fs.writeFile(path + fileName, baseFile, "base64", function (err) {
      if (err) {
        console.log(err);
        reject(err);
      }
      const relativePath = path.replace(env.projectPath, "");
      resolve(relativePath + fileName);
    });
  });
};

module.exports.saveVideo = (fieldName, folderName) => {
  let path = this.getPath("videos", folderName);

  if (!path || path[0] == 'undefined') reject(err);
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true }, (err) => {
      reject(err);
    });
  }

  // SET STORAGE
  var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path);
    },
    filename: function (req, file, cb) {
      const fileName = date.getTimeStamp();
      if (file.mimetype === "video/mp4") {
        cb(null, fileName + ".mp4");
      } else {
        cb(
          new Error(
            "Invalid Format !!!, Please upload video in mp4 format only !!!"
          )
        );
      }
    },
  });
  return multer({ storage: storage }).single(fieldName);
};

module.exports.getPath = (fileType, folderName) => {
  try {
    let path = "";
    if (env.prod === 0) {
      path =
        "files/test/" +
        fileType +
        "/" +
        folderName +
        "/" +
        date.getFullDate() +
        "/" +
        date.getHour() +
        "/";
    } else if (env.prod === 1) {
      path =
        env.projectPath +
        "files/test/" +
        fileType +
        "/" +
        folderName +
        "/" +
        date.getFullDate() +
        "/" +
        date.getHour() +
        "/";
    } else if (env.prod === 2) {
      path =
        env.projectPath +
        "files/prod/" +
        fileType +
        "/" +
        folderName +
        "/" +
        date.getFullDate() +
        "/" +
        date.getHour() +
        "/";
    }
    return path;
  } catch (error) {
    throw error;
  }
};


module.exports.getAbsolutePath = (relativePath) => {
  try {
    let path = "";
    if (env.prod === 0) {
      path = relativePath;
    } else if (env.prod === 1) {
      path =
        env.projectPath + relativePath;
    } else if (env.prod === 2) {
      path =
        env.projectPath + relativePath;
    }
    return path;
  } catch (error) {
    throw error;
  }
}

module.exports.fileDelete = (path) => {
  return new Promise((resolve, reject) => {
    fs.unlink(path, function (error) {
      if (error) {
        resolve(error);
      }
      resolve('1');
    });
  });

}
