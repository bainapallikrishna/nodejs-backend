"use strict"

module.exports.getBatchId = (batchId) => {

    return this.createBatchId(batchId);
}

module.exports.createBatchId = (batchId) => {

    return new Promise((resolve, reject) => {
        try {
            // const fisrtTwoChar = batchId.slice(0, -3);
            // const middleNo = batchId.substring(batchId.length - 3, batchId.length - 2);
            // const lastTwoChar = batchId.slice(-2);

            // if (lastTwoChar === "ZZ") {
            //     if (parseInt(middleNo) + 1 > 9) {
            //         return resolve(this.getNextKey(fisrtTwoChar) + (0).toString() + "AA");
            //     } else {
            //         return resolve(fisrtTwoChar + (parseInt(middleNo) + 1).toString() + "AA");
            //     }
            // } else {
            //     return resolve(fisrtTwoChar + middleNo + this.getNextKey(lastTwoChar));
            // }
            const newBatchId = this.getNextKey(batchId);
            return resolve(newBatchId);
        } catch (error) {
            console.log(error);
            reject(error);
        }
    });

}



// module.exports.getNextKey = (key) => {
//     if (key === 'Z' || key === 'z') {
//         return String.fromCharCode(key.charCodeAt() - 25) + String.fromCharCode(key.charCodeAt() - 25);
//     } else {
//         var lastChar = key.slice(-1);
//         var sub = key.slice(0, -1);
//         if (lastChar === 'Z' || lastChar === 'z') {
//             // If a string of length > 1 ends in Z/z,
//             // increment the string (excluding the last Z/z) recursively,
//             // and append A/a (depending on casing) to it
//             return this.getNextKey(sub) + String.fromCharCode(lastChar.charCodeAt() - 25);
//         } else {
//             // (take till last char) append with (increment last char)
//             return sub + String.fromCharCode(lastChar.charCodeAt() + 1);
//         }
//     }
// };

module.exports.getNextKey = (str) => {
    var alphabet = 'abcdefghijklmnopqrstuvwxyz',
        length = alphabet.length,
        result = str,
        i = str.length;

    while (i >= 0) {
        var last = str.charAt(--i),
            next = '',
            carry = false;

        if (isNaN(last)) {
            const index = alphabet.indexOf(last.toLowerCase());

            if (index === -1) {
                next = last;
                carry = true;
            }
            else {
                var isUpperCase = last === last.toUpperCase();
                next = alphabet.charAt((index + 1) % length);
                if (isUpperCase) {
                    next = next.toUpperCase();
                }

                carry = index + 1 >= length;
                if (carry && i === 0) {
                    var added = isUpperCase ? 'A' : 'a';
                    result = added + next + result.slice(1);
                    break;
                }
            }
        }
        else {
            next = +last + 1;
            if (next > 9) {
                next = 0;
                carry = true;
            }

            if (carry && i === 0) {
                result = '1' + next + result.slice(1);
                break;
            }
        }

        result = result.slice(0, i) + next + result.slice(i + 1);
        if (!carry) {
            break;
        }
    }
    return result;
}


module.exports.getSerialNo = (input) => {

    const randomNum = (Math.floor(Math.random() * 9999) + 1111).toString().split('');
    let chunks = [];

    for (var i = 0, charsLength = input.length; i < charsLength; i += 2) {
        chunks.push(input.substring(i, i + 2));
    }

    let result = '';
    for (let i = 0; i < chunks.length; i++) {
        result += chunks[i].toString() + randomNum[i].toString();
    }
    return result;

}
module.exports.getUniqueId = (input) => {
    const randomNum = (Math.floor(Math.random() * 9999) + 1111).toString().split('');
    let chunks = [];

    for (var i = 0, charsLength = input.length; i < charsLength; i += 3) {
        chunks.push(input.substring(i, i + 3));
    }
    
    let result = '';
    for (let i = 0; i < chunks.length; i++) {
        result += chunks[i].toString() + randomNum[i].toString();
    }

    return result;
}