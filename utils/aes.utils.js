"use strict";
const CryptoJS = require("./crypto.utils");
const env = require("./../config/env.config");

module.exports.encrypt = (input) => {
  const keyVal = CryptoJS.enc.Utf8.parse(env.aesKEY);
  const ivVal = CryptoJS.enc.Utf8.parse(env.aesIV);
  const encrypted = CryptoJS.AES.encrypt(
    CryptoJS.enc.Utf8.parse(input),
    keyVal,
    {
      keySize: 128 / 8,
      iv: ivVal,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    }
  ).toString();
  return encrypted;
};

module.exports.decrypt = (input) => {
  const keyVal = CryptoJS.enc.Utf8.parse(env.aesKEY);
  const ivVal = CryptoJS.enc.Utf8.parse(env.aesIV);
  const decrypted = CryptoJS.AES.decrypt(input, keyVal, {
    keySize: 128 / 8,
    iv: ivVal,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  }).toString(CryptoJS.enc.Utf8);
  return decrypted;
};
