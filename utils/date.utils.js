"use strict";

module.exports.getFullDate = () => {
    const date = new Date();
  const day =
    date.getDate().toString().length === 1
      ? "0" + date.getDate().toString()
      : date.getDate().toString();
  const month =
    date.getMonth().toString().length === 1
      ? "0" + (date.getMonth() + 1).toString()
      : date.getMonth().toString();
  return  date.getFullYear() + month + day;
};

module.exports.getFullYear = () => {
    const date = new Date();
  return date.getFullYear().toString();
};

module.exports.getHour = () => {
    const date = new Date();
  return date.getHours().toString();
};

module.exports.getTimeStamp = () => {
    const date = new Date();
  const day =
    date.getDate().toString().length === 1
      ? "0" + date.getDate().toString()
      : date.getDate().toString();
  const month =
    date.getMonth().toString().length === 1
      ? "0" + (date.getMonth() + 1).toString()
      : date.getMonth().toString();
  const year = date.getFullYear();
  const hour = date.getHours().toString();
  const minutes = date.getMinutes().toString();
  const milliSeconds = date.getMilliseconds().toString();
  return year + month + day + hour + minutes + milliSeconds;
};
