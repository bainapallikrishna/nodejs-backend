'use strict';
const mysql = require('mysql');
const connection = mysql.createPool({
  host: '35.154.197.17',
  port: 3306,
  connectionLimit: 10,
  queueLimit: 20,
  user: 'authbook',
  password: 'Password@123',
  database: 'authbook',
});

module.exports.connection = async (query) => {
  return new Promise((resolve, reject) => {
    connection.query(query, (error, result, fields) => {
      if (error) {
        reject(error);
      } else {
        const json = JSON.parse(JSON.stringify(result))[0];
        resolve(json);
      }
    });
  });
};
