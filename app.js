'use strict';

const path = require('path');
const env = require('./config/env.config');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const helmet = require('helmet');
const jwtAuth = require('./middleware/jwt-auth');


app.use(bodyParser.json({ limit: '30mb' })); // to parse json data
app.use(bodyParser.urlencoded({ limit: '30mb', extended: false })); // to parse x-www-form-urlencoded data
app.use(helmet());
app.use('/files', express.static(path.join(__dirname, 'files')));
app.use(express.static(path.join(__dirname, 'public')));


// const procInput = { p_name: 'Hello' };
// var query = connection.query(
//   'call authbook.test_update('5','' + procInput.p_name + '')',
//   procInput,
//   function (err, result, fields) {
//     if (err) {
//       console.log('err:', err);
//     } else {
//       console.log('results:', result.affectedRows);
//     }
//   }
// );


/*

 app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept'); // cors header
  if(req.method == 'OPTIONS'){
          // In very simple terms, this is how you handle OPTIONS request in nodejs
          res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE, HEAD, PATCH');
          res.header('Access-Control-Max-Age', '1728000');
          res.header('Access-Control-Allow-Credentials', 'true');
          res.header('Access-Control-Allow-Headers', 'Origin,Content-Type,Accept,Authorization, X-AUTH-TOKEN');
          res.header('Content-Type',  'text/plain; charset=UTF-8');
          res.header('Content-Length', '0');
          res.sendStatus(208);
  }
  else{
      next();
  }
//    next();
});

*/

/*

app.use((req, res, next) => {
  console.log('Node Started...');
  next();
});

*/


// routes
app.use('/test', require('./routes/test.routes'));
app.use('/login', require('./routes/login.routes'));
app.use('/user', jwtAuth, require('./routes/user.routes'));
app.use('/publisher', jwtAuth, require('./routes/publisher.routes'));
app.use('/shared', jwtAuth, require('./routes/shared.routes'));

//Global Error Handling
app.use((error, req, res, next) => {
  const status = error.status || 500;
  const message = error;
  res.status(status).json({
    message: message,
  });
});

// Setup a default catch-all route
app.get('*', (req, res) =>
  res.status(200).send({
    message: 'API Not Found',
  })
);

const server = app.listen(
  env.port,
  console.log(`server listening to ${env.port}`)
);
server.timeout = 120000;
