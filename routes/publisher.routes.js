'use strict';
var express = require('express');
var router = express.Router();
const files = require("./../utils/files.utils");
const controller = require('../controllers/publisher.controller');


router.get('/getBooksList', controller.getBooksList);
router.post('/authorSub', controller.authorSub);
router.get('/authorList', controller.authorList);
router.get('/languageMaster', controller.languageMaster);
router.post('/bookSub', controller.bookSub);
router.post('/bookQRGenerationSub', controller.bookQRGenerationSub);
router.post('/batchListByBookId', controller.batchListByBookId);
router.post('/QRCodesListByBatchId', controller.QRCodesListByBatchId);
router.post('/imageUpload', controller.imageUpload);
router.post('/docUpload', controller.docUpload);
router.post('/videoUpload', files.saveVideo("videoFile", "booksVideos"), controller.videoUpload);
router.post('/filesListByBookId', controller.filesListByBookId);
router.post('/bookListByPublisherId', controller.bookListByPublisherId);
router.post('/certificateDetailsByUniqueId', controller.certificateDetailsByUniqueId);

//Upload cover pic of the Book
router.post('/coverPicsUpload', controller.coverPicsUpload);

//Delete Media Fileas
router.post('/deleteFileById', controller.deleteFileById);

//Recent 10 comments
router.post('/commentsList', controller.commentsList);

module.exports = router;