"use strict";
var express = require("express");
var router = express.Router();
const controller = require("../controllers/shared.controller");

router.get("/getVideo", controller.getVideo);
router.post("/profilePicUpdateByUserId", controller.profilePicUpdateByUserId);

module.exports = router;
