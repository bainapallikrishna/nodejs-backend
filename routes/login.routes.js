'use strict';
var express = require('express');
var router = express.Router();

const controller = require('../controllers/login.controller');


router.post('/getBookDetailsByUniqueId',controller.getBookDetailsByUniqueId);
router.post('/verifyUser',controller.verifyUser);
router.post('/sendOtp',controller.sendOtp);
router.post('/verifyOtp',controller.verifyOtp);
router.post('/userLogin',controller.userLogin);
router.get('/stateList',controller.stateList);

module.exports = router;
