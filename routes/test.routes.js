"use strict";
var express = require("express");
var router = express.Router();
const controller = require("../controllers/test.controller");
const files = require("./../utils/files.utils");

router.post("/encrypt", controller.encrypt);
router.post("/decrypt", controller.decrypt);
router.post("/saveImage", controller.saveImage);
router.post("/videoUpload", files.saveVideo("videoFile","booksVideos"), controller.videoUpload);
router.post("/getBatchId", controller.getBatchId);
router.get("/getVideo", controller.getVideo);
router.post('/filesListByBookId',controller.filesListByBookId);

module.exports = router;
