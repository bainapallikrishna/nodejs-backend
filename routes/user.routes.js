'use strict';
var express = require('express');
var router = express.Router();

const controller = require('../controllers/user.controller');


router.post('/userRegSub', controller.userRegSub);
router.post('/userPurchasedBooksList', controller.userPurchasedBooksList);
router.post('/registerBookToUser', controller.registerBookToUser);
router.post('/passwordUpdate', controller.passwordUpdate);
router.post('/certificateDetailsByUniqueId', controller.certificateDetailsByUniqueId);
router.post('/filesListByBookId', controller.filesListByBookId);
router.post('/ratingSub', controller.ratingSub);
router.post('/commentSub', controller.commentSub);
router.post('/ratingUpdate', controller.ratingUpdate);
router.post('/commentUpdate', controller.commentUpdate);

//Recent 10 comments
router.post('/commentsList', controller.commentsList);

module.exports = router;